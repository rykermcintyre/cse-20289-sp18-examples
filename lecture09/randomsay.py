#!/usr/bin/env python3

# Variant 1: check exit status of os.system

import os
import random
import sys


CHARACTERS = []
for index, line in enumerate(os.popen('cowsay -l')):
    if index:
        for character in line.split():
            CHARACTERS.append(character)

SELECTED = random.choice(CHARACTERS)
os.system('cowsay -f {}'.format(SELECTED))
