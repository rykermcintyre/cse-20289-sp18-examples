Lecture 13
==========

# Compiling

## Write a basic "Hello, World!" program:

    #include <stdio.h>

    int main(int argc, char *argv[]) {
        printf("Hello, World!\n");
        return 0;
    }

## Compile the "Hello, World!" program:

    $ gcc -o hello.exe hello.c                  # Discuss: a.out if -o not specified

    $ file hello.exe
    $ hexdump -C hello.exe                      # Discuss: binary executable
    $ readelf -a hello.exe                      # Discuss: ELF file format

## Compiler Pipeline

    $ cpp hello.c > hello_.c                    # 1. Pre-process: Source -> Source
                                                #   Macros, Directives

    $ gcc -S -o hello.s hello_.c                # 2. Compile:     Source -> Assembly
                                                #   Check syntax, Generate assembly,
                                                #   Optimization

    $ as -o hello.o hello.s                     # 3. Assemble:    Assembly -> Object
                                                #   Check Syntax, Generate object,
                                                #   Optimization

    $ gcc -o hello.exe hello.o                  # 4. Link:        Object -> Executable
                                                #   Package object with libraries

## Dynamic vs Static Static Executables

    $ ldd hello.exe                             # Discuss: Dynamic linking
    $ strace hello.exe                          # Discuss: System calls (briefly)
    $ gcc -static -o hello.static hello.o       # Discuss: ls, file, ldd, strace

## Shared vs Static Libraries

We factor `hello.c` into library and driver files:

    /* library.c */
    #include <stdio.h>

    void greet(const char *name) {
        printf("Hello, %s\n", name);            # Discuss: -Wall
    }

    /* main.c */
    extern void greet(char *name);              # Discuss: prototype, header

    int main(int argc, char *argv[]) {
        for (int i = 1; i < argc; i++) {        # Discuss: -std=gnu99
            greet(argv[i]);
        }
        return 0;
    }

Create `Makefile` to build project

    all:    greet.dynamic greet.static

    library.o:	    library.c
            gcc -c -fPIC -o library.o library.c # Discuss: -c, -fPIC

    main.o:	    main.c
            gcc -c -o main.o main.c

    libgreet.so:    library.o
            gcc -shared -o libgreet.so library.o

    libgreet.a:	    library.o
            ar rcs libgreet.a library.o

    greet.dynamic:  main.o libgreet.so
            gcc -L. -o greet.dynamic main.o -lgreet

    greet.static:   main.o libgreet.a
            gcc -o greet.static main.o libgreet.a

    clean:
            rm -f greet.dynamic greet.static *.o libgreet.*

Use variables, pattern rules, and automatic variables in `Makefile`

    CC=		gcc                             # Discuss: Variables
    CFLAGS=     -Wall -std=gnu99 -g -gdwarf-2
    LD=		gcc
    LDFLAGS=	-L.
    LIBS=	-lgreet
    AR=		ar
    ARFLAGS=	rcs
    TARGETS=	greet.dynamic greet.static

    all:		$(TARGETS)

    %.o:		%.c                     # Discuss: Pattern Rule
            $(CC) $(CFLAGS) -fPIC -c -o $@ $^   # Discuss: Automatic Variables

    libgreet.so:	library.o
            $(LD) -shared -o $@ $^

    libgreet.a:	library.o
            $(AR) $(ARFLAGS) $@ $^

    greet.dynamic:	main.o libgreet.so
            $(LD) $(LDFLAGS) -o $@ $< $(LIBS)

    greet.static:	main.o libgreet.a
            $(LD) $(LDFLAGS) -static -o $@ $< $(LIBS)

    clean:
            rm -f $(TARGETS) *.o libgreet.*
