#!/usr/bin/env python3

import sys

for n in [x for x in range(int(sys.argv[1]))]:
    print(n)
