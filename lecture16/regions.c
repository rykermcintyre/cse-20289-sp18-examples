/* regions.c */

#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

typedef struct {
    double x;
    double y;
} Point;

typedef union {
    char   c;
    int    i;
    float  f;
    double d;
} Block;

double GD = 3.14;

int main(int argc, char *argv[]) {
    int        a[] = {4, 6, 6, 3, 7};
    char      *s   = "Happy Kid";
    Block      b   = {0};

    Point     p0   = {0, 0};
    Point    *p1   = NULL;
    Point    *p2   = malloc(sizeof(Point));
    Point    *p3   = malloc(10*sizeof(Point));
    Point   **p4   = malloc(10*sizeof(Point *));
    static int t   = 2;

    printf("  a. Address: 0x%012lx, Size: %lu\n", (uintptr_t) &a, sizeof(a));
    printf("  s. Address: 0x%012lx, Size: %lu\n", (uintptr_t)  s, sizeof(s));
    printf("&sp. Address: 0x%012lx, Size: %lu\n", (uintptr_t) &s, sizeof(&s));
    printf("  b. Address: 0x%012lx, Size: %lu\n", (uintptr_t) &b, sizeof(b));
    puts("");
    printf(" p0. Address: 0x%012lx, Size: %lu\n", (uintptr_t)&p0, sizeof(p0));
    printf(" p1. Address: 0x%012lx, Size: %lu\n", (uintptr_t) p1, sizeof( p1));
    printf("&p1. Address: 0x%012lx, Size: %lu\n", (uintptr_t)&p1, sizeof(&p1));
    printf(" p2. Address: 0x%012lx, Size: %lu\n", (uintptr_t) p2, sizeof( p2));
    printf("&p2. Address: 0x%012lx, Size: %lu\n", (uintptr_t)&p2, sizeof(&p2));
    printf(" p3. Address: 0x%012lx, Size: %lu\n", (uintptr_t) p3, sizeof( p3));
    printf("&p3. Address: 0x%012lx, Size: %lu\n", (uintptr_t)&p3, sizeof(&p3));
    printf(" p4. Address: 0x%012lx, Size: %lu\n", (uintptr_t) p4, sizeof( p4));
    printf("&p4. Address: 0x%012lx, Size: %lu\n", (uintptr_t)&p4, sizeof(&p4));
    puts("");
    printf(" GD. Address: 0x%012lx, Size: %lu\n", (uintptr_t)&GD, sizeof(GD));
    printf("  t. Address: 0x%012lx, Size: %lu\n", (uintptr_t) &t, sizeof(t));

    /* Demonstrations
     *
     * - Segfault if we do `s[1] = 'b'`.  No segfault if we change *s to s[].
     *
     *	    Arrays are LIKE pointers, are not actually pointers (can't do
     *	    assignment, don't take up their own space).
     *
     * - Running repeatedly will show that the stack and heap allocations
     *   change, but not the code and data variables do not.
     */
    return EXIT_SUCCESS;
}
