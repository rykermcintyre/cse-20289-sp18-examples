#!/usr/bin/env python3

import re

def match(pattern, iterable):
    for item in iterable:
        if re.search(pattern, item):
            yield item

'''
def match(pattern, iterable):
    return (item for item in iterable if re.search(pattern, item))
'''

matches = match(r'\d{3}', ['abc', '1', '123', 'abc123'])
print(matches)                                  # Discuss: Doesn't do anything

for m in matches:                               # Discuss: Consume generator
    print(m)
