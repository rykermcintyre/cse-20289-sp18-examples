#!/usr/bin/env python3

import sys

for line in sys.stdin:
    for word in line.strip().split():
        print('{}\t{}'.format(word, 1))

# vim: set sts=4 sw=4 ts=8 expandtab ft=python:
