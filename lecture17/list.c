/* list.c: Deque-based List */

#include "list.h"

#include <assert.h>

/**
 * Create List structure.
 * @param   capacity    Internal capacity.
 * @param   type        Value type.
 * @return  Newly allocated List structure with specified capacity.
 */
List *	list_create(size_t capacity, Type type) {
    List *l = calloc(1, sizeof(List));
    if (l) {
        l->type     = type;
        l->capacity = capacity;
        l->data     = calloc(l->capacity, sizeof(Value));
        if (l->data == NULL) {
            free(l);
            return NULL;
        }
    }
    return l;
}

/**
 * Delete List structure.
 * @param   l           List structure.
 * @return  NULL.
 */
List *	list_delete(List *l) {
    for (List *curr = l, *next = NULL; curr; curr = next) {
        next = curr->next;
        free(curr->data);
        free(curr);
    }
    return NULL;
}

/**
 * Search List structure.
 * @param   l           List structure.
 * @param   value       Value to search for.
 * @return  Whether or not the value is in the List structure.
 */
bool    list_search(const List *l, const Value value) {
    for (const List *curr = l; curr; curr = curr->next) {
        for (size_t v = 0; v < curr->size; v++) {
            if (curr->data[v].number == value.number)
                return true;
        }
    }

    return false;
}

/**
 * Update List structure.
 * @param   l           List structure.
 * @param   value       New value.
 */
void    list_insert(List *l, Value value) {
    while (l->next) {
        l = l->next;
    }

    if (l->size == l->capacity) {
        l->next = list_create(l->capacity, l->type);
        l       = l->next;
    }

    l->data[l->size++] = value;
}

/**
 * Print List Structure.
 * @param   l           List structure.
 * @param   stream      I/O stream to write to.
 */
void	list_fprint(const List *l, FILE *stream) {
    for (const List *curr = l; curr; curr = curr->next) {
        for (size_t v = 0; v < curr->size; v++) {
            switch (l->type) {
                case LETTER: fprintf(stream, "v: %c\n", curr->data[v].letter); break;
                case NUMBER: fprintf(stream, "v: %d\n", curr->data[v].number); break;
            }
        }
    }
}

/* Constants */

static const char *DATA = "0123456789";

/* Main Execution */

int main(int argc, char *argv[]) {
    List *l = list_create(4, LETTER);

    for (const char *c = DATA; *c; c++) {
        Value v = {*c};
        list_insert(l, v);
    }

    list_fprint(l, stdout);
    
    for (const char *c = DATA; *c; c++) {
        Value v = {*c};
        assert(list_search(l, v));
    }

    list_delete(l);

    return 0;
}

/* vim: set sts=4 sw=4 ts=8 expandtab ft=c: */
